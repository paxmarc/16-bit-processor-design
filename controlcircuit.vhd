LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY controlCircuit IS
    PORT( func : IN STD_LOGIC_VECTOR(2 downto 0);
    CLK, RESET, w: IN STD_LOGIC;
    Rx, Ry : IN STD_LOGIC_VECTOR(2 downto 0);
    RIN : OUT STD_LOGIC_VECTOR(7 downto 0);
    ROUT : OUT STD_LOGIC_VECTOR(7 downto 0);
    Ain, Aout, Gin, Gout, PCin, PCout : OUT STD_LOGIC;
    Done : OUT STD_LOGIC;
    DATA : OUT STD_LOGIC;
    ALUOp : OUT STD_LOGIC_VECTOR(2 downto 0)
);
END controlCircuit;

ARCHITECTURE Behaviour of controlCircuit IS
    TYPE State_type IS (Idle, Load, MOV, Add1, Add2, Add3, Sub1, Sub2, Sub3, xor1, xor2, xor3, loadpc, branch);
    SIGNAL S_Next, S_Present : State_type := Idle;
    SIGNAL RxNum, RyNum : natural range 0 to 7;
    signal RxSig, RySig : unsigned(2 downto 0); 

BEGIN
		
    PROCESS(CLK, w)
    BEGIN
        IF rising_edge(CLK) AND w = '1' THEN
            S_present <= S_next;
        END IF;
    END PROCESS;
	 
    PROCESS(S_present, Rx, Ry, CLK) 
    BEGIN
        IF S_present = Idle AND rising_edge(CLK) THEN
            RxNum <= to_integer(RxSig);
            RyNum <= to_integer(RySig);
        END IF;
    END PROCESS;

    PROCESS (S_present, func, RESET)
    BEGIN
	 
        CASE S_present IS
            WHEN Idle => -- Waiting for input
                Ain <= '0';
                Aout <= '0'; 
                Gin <= '0'; 
                Gout <= '0';
                PCin <= '0';
                PCout <= '0';
                RIN <= (OTHERS => '0');
                ROUT <= (OTHERS => '0');
                DATA <= '0';
                DONE <= '0';
                IF func = "000" THEN
                    S_Next <= Load;
						  DONE <= '1';
                ELSIF func = "001" THEN
                    S_Next <= MOV;
                ELSIF func = "010" THEN
                    S_Next <= Add1;
                ELSIF func = "011" THEN
                    S_Next <= Sub1;
                ELSIF func = "100" THEN
                    S_Next <= xor1;
                ELSIF func = "101" THEN
                    S_Next <= loadpc;
                ELSIF func = "110" THEN
                    S_Next <= branch;
                ELSE
                    S_Next <= Idle;
                END IF;
            WHEN Load => -- Load an external data value into the specified register Rx
                RIN <= (OTHERS => '0');
                ROUT <= (OTHERS => '0');
                RIN(RxNum) <= '1';
                DATA <= '1';
                DONE <= '1';
                S_Next <= Idle;
            WHEN MOV => -- Copy the value of Ry to Rx
                RIN <= (OTHERS => '0');
                ROUT <= (OTHERS => '0');
                ROUT(RyNum) <= '1';
                RIN(RxNum) <= '1';
                DONE <= '1';
                S_Next <= Idle;
            WHEN Add1 => -- Add the values of Rx and Ry and store again in Rx
                RIN <= (OTHERS => '0');
                ROUT <= (OTHERS => '0');
                ROUT(RxNum) <= '1';
                Ain <= '1';
                S_Next <= Add2;
            WHEN Add2 =>
                RIN <= (OTHERS => '0');
                ROUT <= (OTHERS => '0');
					 Ain <= '0';
                Gin <= '1';
                ROUT(RyNum) <= '1';
                ALUOp <= "010";
                S_Next <= Add3;
            WHEN Add3 =>
                RIN <= (OTHERS => '0');
                ROUT <= (OTHERS => '0');
                Gin <='0';
                Gout <= '1';
                RIN(RxNum) <= '1';
                Done <= '1';
                S_Next <= Idle;
            WHEN Sub1 => -- Subract the values of Rx and Ry and store again in Rx
                RIN <= (OTHERS => '0');
                ROUT <= (OTHERS => '0');
                ROUT(RxNum) <= '1';
                Ain <= '1';
                S_Next <= Sub2;
            WHEN Sub2 =>
                RIN <= (OTHERS => '0');
                ROUT <= (OTHERS => '0');
					 Ain <= '0';
                Gin <= '1';
                ROUT(RyNum) <= '1';
                ALUOp <= "011";
                S_Next <= Sub3;
            WHEN Sub3 =>
                RIN <= (OTHERS => '0');
                ROUT <= (OTHERS => '0');
                Gin <='0';
                Gout <= '1';
                RIN(RxNum) <= '1';
                Done <= '1';
                S_Next <= Idle;
            WHEN xor1 => -- XOR the values of Rx and Ry and store again in Rx
                RIN <= (OTHERS => '0');
                ROUT <= (OTHERS => '0');
                ROUT(RxNum) <= '1';
                Ain <= '1';
                S_Next <= xor2;
            WHEN xor2 =>
                RIN <= (OTHERS => '0');
                ROUT <= (OTHERS => '0');
					 Ain <= '0';
                Gin <= '1';
                ROUT(RyNum) <= '1';
                ALUOp <= "100";
                S_Next <= xor3;
            WHEN xor3 =>
                RIN <= (OTHERS => '0');
                ROUT <= (OTHERS => '0');
                Gin <='0';
                Gout <= '1';
                RIN(RxNum) <= '1';
                Done <= '1';
                S_next <= Idle;
            WHEN loadpc =>
                RIN <= (OTHERS => '0');
                ROUT <= (OTHERS => '0');
                RIN(RxNum) <= '1';
                PCout <= '1';
                Done <= '1';
                S_next <= Idle;
            WHEN branch =>
                RIN <= (OTHERS => '0');
                ROUT <= (OTHERS => '0');
                PCin <= '1';
                ROUT(RxNum) <= '1';
                Done <= '1';
                S_next <= Idle;
        END CASE;
    END PROCESS;

    RxSig <= unsigned(Rx);
    RySig <= unsigned(Ry);

END Behaviour;
