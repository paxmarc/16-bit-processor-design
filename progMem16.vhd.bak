LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY progMem16 IS
GENERIC ( S : INTEGER;
          N : INTEGER);
	-- S is the memory size (number of words, must be less than or equal to 2^N), 
        -- N is the address size (number of bits)
        -- number of data (q) bits is fixed at 16
port(
  clock        : in std_logic;
  data         : in std_logic_vector (15 downto 0);
  write_addr   : in std_logic_vector (N-1 downto 0);
  read_addr    : in std_logic_vector (N-1 downto 0);
  write_enable : in std_logic := '0';
  q            : out std_logic_vector (15 downto 0));
END;

ARCHITECTURE behavioral OF progMem16  IS
  TYPE mem IS ARRAY (S-1 downto 0) OF std_logic_vector(15 downto 0);

  FUNCTION initialize_ram RETURN mem IS
  VARIABLE result : mem;
  BEGIN
	result(0) := "0000000000110000"; -- LOAD R3
	result(1) := "0000000000000001"; -- value 1 to be loaded
	result(2) := "0000000000010000"; -- LOAD R1
	result(3) := "0000000000000101";-- VALUE 5 to be loaded
	result(4) := "0100000000110001"; -- Add R3, R1
	result(5) := "0110000000110001"; -- SUB R3, R1
	result(6) := "1000000000110001"; -- XOR R3, R1
	result(7) := "0000000001110000"; -- LOAD R7
	result(8) := "1000000000000000"; -- VALUE 0x8000
	result(9) := "0010000001110011"; -- MOV R3, R7
    return result;
  END initialize_ram;

  SIGNAL raMem : mem := initialize_ram;
BEGIN
  PROCESS (clock, read_addr(N-1), raMem)
  BEGIN
    IF rising_edge(clock) THEN
      IF write_enable = '1' THEN
        raMem(to_integer(unsigned(write_addr))) <= data;
      END IF;
    END IF;
	 
	 q <= raMem(to_integer(unsigned(read_addr)));
  END PROCESS;
END behavioral;
