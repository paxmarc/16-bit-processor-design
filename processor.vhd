LIBRARY ieee;
USE ieee.std_logic_1164.all;
entity processor IS
    PORT ( 	Data : IN STD_LOGIC_VECTOR(15 downto 0);
    Reset, w : IN STD_LOGIC;
    Clk : IN STD_LOGIC;
    F : IN STD_LOGIC_VECTOR(2 downto 0);
    Rx, Ry : IN STD_LOGIC_VECTOR(2 downto 0);
    Done : OUT STD_LOGIC;
    BusWires : INOUT STD_LOGIC_VECTOR(15 downto 0);
	 reg3Out : OUT STD_LOGIC_VECTOR(15 downto 0);
	 PCOut : OUT std_LOGIC_VECTOR(7 downto 0));
end processor;

ARCHITECTURE structure of processor IS
    
	 COMPONENT tristate is
        port( A : in std_logic_vector(15 downto 0); 
              enable : in std_logic; 
              Q : out std_logic_vector(15 downto 0)); 
    END COMPONENT;
    
	 COMPONENT alu is
        PORT (S : IN std_logic_vector(2 downto 0);
        A, B : IN std_logic_vector(15 downto 0);
        Gout : OUT std_logic_vector(15 downto 0)); --Need to insert I/O signals once they are done
    END COMPONENT;
    
	 COMPONENT controlCircuit IS
        port (func : IN STD_LOGIC_VECTOR(2 downto 0);
        CLK, RESET, w : IN STD_LOGIC;
        Rx, Ry : IN STD_LOGIC_VECTOR(2 downto 0);
        RIN : OUT STD_LOGIC_VECTOR(7 downto 0);
        ROUT : OUT STD_LOGIC_VECTOR(7 downto 0);
        Ain, Aout, Gin, Gout, PCin, PCout : OUT STD_LOGIC;
        Done : OUT STD_LOGIC;
        DATA : OUT STD_LOGIC;
        ALUOp : OUT STD_LOGIC_VECTOR(2 downto 0)); --Need to insert I/O signals
    END COMPONENT;
    
	 COMPONENT registerA IS
        port( CLK, RIN : IN STD_LOGIC;
              busIn : IN STD_LOGIC_VECTOR(15 downto 0);
              output : OUT STD_LOGIC_VECTOR(15 downto 0));
    END COMPONENT;
	 
	 COMPONENT program_counter IS
		GENERIC (S : INTEGER; 
			N : INTEGER);
				-- N is the address width 
				-- S is the number of instructions;
		PORT ( done, clk, reset, busIn : IN STD_LOGIC;
        addressIn : IN STD_LOGIC_VECTOR(15 downto 0);
        addressOut : OUT STD_LOGIC_VECTOR(15 downto 0);
		address : OUT STD_LOGIC_VECTOR(N-1 downto 0);
		finish : OUT STD_LOGIC);
		END COMPONENT;

    --register outputs
    SIGNAL AO, GO, PCO : STD_LOGIC_VECTOR(15 downto 0); -- Signals of output from registers A and G
    SIGNAL ALUOut : STD_LOGIC_VECTOR(15 downto 0); -- Signal output from the ALU - the mathematical operation
    SIGNAL R0, R1, R2, R3, R4, R5, R6, R7 : STD_LOGIC_VECTOR(15 downto 0); -- R8, R9, R10, R11, R12, R13, R14, R15 : STD_LOGIC_VECTOR(15 downto 0); -- The signals coming out of each register
    SIGNAL RinS, RoutS: STD_LOGIC_VECTOR(7 downto 0); -- Vector of register read-enable signals and write-enable signals.
    SIGNAL AinS, AoutS, GinS, GoutS, PCinS, PCoutS : STD_LOGIC; -- 
    SIGNAL DoneS : STD_LOGIC;
    SIGNAL DataS : STD_LOGIC;
    SIGNAL ALU_operation : STD_LOGIC_VECTOR(2 downto 0);
	SIGNAL PC_finish : STD_LOGIC;
	 
	 -- Keep signals for debugging purposes.
    ATTRIBUTE keep : boolean;
    ATTRIBUTE keep of R0, R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, R12, R13, R14, R15, AO, GO : SIGNAL IS true;


--all that's left to do is instantiate the components and link them together once they are finished.
BEGIN
	-- FIRST GENERIC IS NUMBER OF INSTRUCTIONS, SECOND IS ADDRESS WIDTH
	 pc1 : program_counter generic map (10, 8) port map (DoneS, Clk, reset, PCinS, BusWires, PCO, PCOut, PC_finish);
    --instantiate registers
    reg0: registerA port map (Clk, RinS(0), BusWires(15 downto 0), R0(15 downto 0));
    reg1: registerA port map (Clk, RinS(1), BusWires(15 downto 0), R1(15 downto 0));
    reg2: registerA port map (Clk, RinS(2), BusWires(15 downto 0), R2(15 downto 0));
    reg3: registerA port map (Clk, RinS(3), BusWires(15 downto 0), R3(15 downto 0));
    reg4: registerA port map (Clk, RinS(4), BusWires(15 downto 0), R4(15 downto 0));
    reg5: registerA port map (Clk, RinS(5), BusWires(15 downto 0), R5(15 downto 0));
    reg6: registerA port map (Clk, RinS(6), BusWires(15 downto 0), R6(15 downto 0));
    reg7: registerA port map (Clk, RinS(7), BusWires(15 downto 0), R7(15 downto 0));
--    reg8: registerA port map (Clk, RinS(8), BusWires(15 downto 0), R8(15 downto 0));
----    reg9: registerA port map (Clk, RinS(9), BusWires(15 downto 0), R9(15 downto 0));
----    reg10: registerA port map (Clk, RinS(10), BusWires(15 downto 0), R10(15 downto 0));
----    reg11: registerA port map (Clk, RinS(11), BusWires(15 downto 0), R11(15 downto 0));
----    reg12: registerA port map (Clk, RinS(12), BusWires(15 downto 0), R12(15 downto 0));
----    reg13: registerA port map (Clk, RinS(13), BusWires(15 downto 0), R13(15 downto 0));
----    reg14: registerA port map (Clk, RinS(14), BusWires(15 downto 0), R14(15 downto 0));
----    reg15: registerA port map (Clk, RinS(15), BusWires(15 downto 0), R15(15 downto 0));

    --instantiate tri-state vectors
    t0: tristate port map (R0(15 downto 0), RoutS(0), BusWires(15 downto 0));
    t1: tristate port map (R1(15 downto 0), RoutS(1), BusWires(15 downto 0));
    t2: tristate port map (R2(15 downto 0), RoutS(2), BusWires(15 downto 0));
    t3: tristate port map (R3(15 downto 0), RoutS(3), BusWires(15 downto 0));
    t4: tristate port map (R4(15 downto 0), RoutS(4), BusWires(15 downto 0));
    t5: tristate port map (R5(15 downto 0), RoutS(5), BusWires(15 downto 0));
    t6: tristate port map (R6(15 downto 0), RoutS(6), BusWires(15 downto 0));
    t7: tristate port map (R7(15 downto 0), RoutS(7), BusWires(15 downto 0));
    tPC: tristate port map(PCO, PCoutS, BusWires);
--    t8: tristate port map (R8(15 downto 0), RoutS(8), BusWires(15 downto 0));
--    t9: tristate port map (R9(15 downto 0), RoutS(9), BusWires(15 downto 0));
--    t10: tristate port map (R10(15 downto 0), RoutS(10), BusWires(15 downto 0));
--    t11: tristate port map (R11(15 downto 0), RoutS(11), BusWires(15 downto 0));
--    t12: tristate port map (R12(15 downto 0), RoutS(12), BusWires(15 downto 0));
--    t13: tristate port map (R13(15 downto 0), RoutS(13), BusWires(15 downto 0));
--    t14: tristate port map (R14(15 downto 0), RoutS(14), BusWires(15 downto 0));
--    t15: tristate port map (R15(15 downto 0), RoutS(15), BusWires(15 downto 0));

    -- 'A' register, as on the spec, holds one value for the ALU to use
    rA: registerA port map (Clk, AinS, BusWires(15 downto 0), AO(15 downto 0));

    c0: controlCircuit port map (F(2 downto 0), Clk, Reset, NOT(PC_finish), Rx(2 downto 0), Ry(2 downto 0), RinS(7 downto 0), RoutS(7 downto 0), AinS, AoutS, GinS, GoutS, PCinS, PCoutS, DoneS, DataS, ALU_operation(2 downto 0));

    a0 : alu port map (ALU_operation, AO, BusWires(15 downto 0), ALUOut(15 downto 0));

    -- ALU Output register G
    rG: registerA port map (Clk, GinS, ALUOut(15 downto 0), GO);
    tG: tristate port map (GO, GoutS, BusWires(15 downto 0));

     -- External data tristate
    tD: tristate port map (Data, DataS, BusWires(15 downto 0));
	 
	 reg3Out <= R3;
	 Done <= DoneS;
	 
	 
				
	 
END structure;
