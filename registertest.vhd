LIBRARY ieee;
USE ieee.std_logic_1164.all;
entity registertest is
port (in1, out1, clk : IN STD_LOGIC;
		in16 : IN STD_LOGIC_VECTOR(15 downto 0);
		out16 : OUT STD_LOGIC_VECTOR(15 downto 0));
end registertest;

architecture structural of registertest is
component register1 is
port( CLK, RIN, ROUT : IN STD_LOGIC;
        busIn : IN STD_LOGIC_VECTOR(15 downto 0);
        busOut : OUT STD_LOGIC_VECTOR(15 downto 0));
end component;

begin

r0 : register1 port map (clk, in1, out1, in16(15 downto 0), out16(15 downto 0));

end structural;