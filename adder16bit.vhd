LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY adder16bit IS
    PORT( A, B : IN STD_LOGIC_VECTOR(15 downto 0);
    Cin : IN STD_LOGIC;
    Cout, overflow : OUT STD_LOGIC;
    Sum : OUT STD_LOGIC_VECTOR(15 downto 0));
END adder16bit;

ARCHITECTURE structural of adder16bit IS
    SIGNAL c : STD_LOGIC_VECTOR(16 downto 0);

    COMPONENT full_adder IS
        PORT ( a, b, cin : IN STD_LOGIC;
        cout, s : OUT std_logic);
    END COMPONENT;

BEGIN
    
    c(0) <= Cin;

    GEN_ADDER:
    FOR I in 0 to 15 GENERATE
        ADDI: full_adder port map (A(I), B(I), c(I), c(I+1), Sum(I));
    END GENERATE GEN_ADDER;

    overflow <= c(15) XOR c(16);

    Cout <= c(16);

END structural;