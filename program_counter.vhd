LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

entity program_counter IS 
GENERIC (S : INTEGER; 
			N : INTEGER);
-- N is the address width 
-- S is the number of instructions;
PORT ( done, clk, reset, busIn : IN STD_LOGIC;
		addressIn : IN STD_LOGIC_VECTOR(15 downto 0);
        addressOut : OUT STD_LOGIC_VECTOR(15 downto 0);
        address : OUT STD_LOGIC_VECTOR(N-1 downto 0);
		finish : OUT STD_LOGIC);
END program_counter;

ARCHITECTURE behavioural of program_counter IS
SIGNAL PC : INTEGER := 0;
SIGNAL running : std_logic := '1';
SIGNAL temp_signal : STD_LOGIC_VECTOR(N-1 downto 0);

BEGIN

PROCESS (done, clk, reset)
BEGIN
	IF rising_edge(clk) AND done = '1' AND running = '1' THEN
		IF reset = '1' THEN
			PC <= 0;
			finish <= '0';
        ELSIF busIn = '1' THEN
            PC <= to_integer(unsigned(addressIn)); 
        ELSIF PC = S-1 THEN
			finish <= '1';
		ELSE 
			PC <= PC + 1;
		END IF;
	END IF;
END PROCESS;

address <= std_logic_vector(to_unsigned(PC,N));
addressOut <= std_logic_vector(to_unsigned(PC,16));

END behavioural;
