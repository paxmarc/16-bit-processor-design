LIBRARY ieee;
USE ieee.std_logic_1164.all;
entity procBoard IS
PORT( SW : IN STD_LOGIC_VECTOR(17 downto 0);
		KEY : IN STD_LOGIC_VECTOR(1 downto 0);
		LEDR : OUT STD_LOGIC_VECTOR(15 downto 0);
		LEDG : OUT STD_LOGIC_VECTOR(0 downto 0);
		HEX0 : OUT STD_LOGIC_VECTOR(0 to 6);
		HEX1 : OUT STD_LOGIC_VECTOR(0 to 6);
		HEX2 : OUT STD_LOGIC_VECTOR(0 to 6);
		HEX3 : OUT STD_LOGIC_VECTOR(0 to 6);
		HEX6 : OUT STD_LOGIC_VECTOR(0 to 6);
		HEX4 : OUT STD_LOGIC_VECTOR(0 to 6));
END procBoard;

ARCHITECTURE structural OF procBoard IS
COMPONENT processor IS 
PORT ( 	Data : IN STD_LOGIC_VECTOR(15 downto 0);
    Reset, w : IN STD_LOGIC;
    Clk : IN STD_LOGIC;
    F : IN STD_LOGIC_VECTOR(2 downto 0);
    Rx, Ry : IN STD_LOGIC_VECTOR(3 downto 0);
    Done : OUT STD_LOGIC;
    BusWires : INOUT STD_LOGIC_VECTOR(15 downto 0);
	 reg3Out : OUT STD_LOGIC_VECTOR(15 downto 0));
END COMPONENT;	 


COMPONENT hexdecode IS
PORT( A : IN std_logic_vector(3 downto 0);
			D : OUT std_logic_vector(0 to 6));
END COMPONENT;

SIGNAL BusSig : STD_LOGIC_VECTOR(15 downto 0);

BEGIN

p0 : processor port map (SW(15 downto 0), SW(17), SW(16), KEY(0), SW(15 downto 13), SW(7 downto 4), SW(3 downto 0), LEDG(0), BusSig(15 downto 0), LEDR(15 downto 0));

h0 : hexdecode port map (BusSig(3 downto 0), HEX0(0 to 6));
h1 : hexdecode port map (BusSig(7 downto 4), HEX1(0 to 6));
h2 : hexdecode port map (BusSig(11 downto 8), HEX2(0 to 6));
h3 : hexdecode port map (BusSig(15 downto 12), HEX3(0 to 6));
h4 : hexdecode port map (SW(3 downto 0), HEX4(0 to 6));
h6 : hexdecode port map (SW(7 downto 4), HEX6(0 to 6));

END structural;