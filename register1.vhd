LIBRARY ieee;
USE ieee.std_logic_1164.all;
entity register1 is
    port( CLK, RIN, ROUT : IN STD_LOGIC;
        busIn : IN STD_LOGIC_VECTOR(15 downto 0);
        busOut : OUT STD_LOGIC_VECTOR(15 downto 0));
end register1;

architecture structural of register1 is
component tristate is
    port ( A : IN std_logic_vector(15 downto 0);
        enable : IN std_logic;
        Q : out std_logic_vector(15 downto 0));
end component;
signal storage : std_logic_vector(15 downto 0);
begin

    process(RIN, CLK)
    begin
        if rising_edge(CLK) AND RIN = '1' THEN
            storage <= busIn;
        end if;
    end process;

    t0: tristate port map (storage(15 downto 0), ROUT, busOut(15 downto 0));

end structural;
