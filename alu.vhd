LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;


ENTITY alu is
    PORT (S : IN std_logic_vector(2 downto 0);
    A, B : IN std_logic_vector(15 downto 0);
    Gout : OUT std_logic_vector(15 downto 0));
END alu;


ARCHITECTURE behaviour of alu is
    SIGNAL temp_one : std_logic_vector(15 downto 0);
    COMPONENT adder16bit IS
        PORT( A, B : IN STD_LOGIC_VECTOR(15 downto 0);
              Cin : IN STD_LOGIC;
        Cout, overflow : OUT STD_LOGIC;
        Sum : OUT STD_LOGIC_VECTOR(15 downto 0));
    END COMPONENT;

    COMPONENT xor_circuit IS
        PORT( A, B : IN STD_LOGIC_VECTOR(15 downto 0);
              xor_out : OUT STD_LOGIC_VECTOR(15 downto 0));
    END COMPONENT;

    SIGNAL G2,G3,G4,Bneg : std_logic_vector(15 downto 0); 
    SIGNAL Cout, Cin : std_logic;

BEGIN

	 Cin <= '0';
    temp_one <= "0000000000000001";
    Bneg <= std_logic_vector(unsigned(NOT(B)) + unsigned(temp_one));
    a1: adder16bit port map(A, B, Cin, Cout, open, G2);
    a2: adder16bit port map(A, Bneg, Cin, Cout, open, G3);
    a3: xor_circuit port map(A, B, G4);

    WITH S select
        Gout <= G2 WHEN "010",
                G3 WHEN "011",
                G4 WHEN "100",
                "0000000000000000" WHEN OTHERS;

END behaviour;
