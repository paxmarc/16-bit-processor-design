LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
entity procBoardWithMem IS -- NEEDS TO DEFAULT TO ADDRESS WIDTH
PORT( KEY : IN STD_LOGIC_VECTOR(1 downto 0);
		LEDR : OUT STD_LOGIC_VECTOR(15 downto 0);
		LEDG : OUT STD_LOGIC_VECTOR(7 downto 0);
		HEX0 : OUT STD_LOGIC_VECTOR(0 to 6);
		HEX1 : OUT STD_LOGIC_VECTOR(0 to 6);
		HEX2 : OUT STD_LOGIC_VECTOR(0 to 6);
		HEX3 : OUT STD_LOGIC_VECTOR(0 to 6);
		HEX6 : OUT STD_LOGIC_VECTOR(0 to 6);
		HEX4 : OUT STD_LOGIC_VECTOR(0 to 6);
		HEX5 : OUT STD_LOGIC_VECTOR(0 to 6);
		HEX7 : OUT STD_LOGIC_VECTOR(0 to 6);
		viewbus : OUT STD_LOGIC_VECTOR(15 downto 0);
	 memview : OUT STD_LOGIC_VECTOR(15 downto 0));
END procBoardWithMem;

ARCHITECTURE structural OF procBoardWithMem IS
COMPONENT processor IS 
 -- Change to width of program counter
PORT ( 	Data : IN STD_LOGIC_VECTOR(15 downto 0);
    Reset : IN STD_LOGIC;
    Clk : IN STD_LOGIC;
    F : IN STD_LOGIC_VECTOR(2 downto 0);
    Rx, Ry : IN STD_LOGIC_VECTOR(2 downto 0);
    Done : OUT STD_LOGIC;
    BusWires : INOUT STD_LOGIC_VECTOR(15 downto 0);
	 reg3Out : OUT STD_LOGIC_VECTOR(15 downto 0);
	 PCOut : OUT std_LOGIC_VECTOR(7 downto 0));
END COMPONENT;	 

COMPONENT progMem16 IS
GENERIC ( S : INTEGER;
          N : INTEGER);
			-- S is the memory size (number of words, must be less than or equal to 2^N), 
			-- N is the address size (number of bits)
			-- number of data (q) bits is fixed at 16
port(
  clock        : in std_logic;
  data         : in std_logic_vector (15 downto 0) := std_LOGIC_VECTOR(to_unsigned(0, 16));
  write_addr   : in std_logic_vector (N-1 downto 0) := std_LOGIC_VECTOR(to_unsigned(0, 8)) ;
  read_addr    : in std_logic_vector (N-1 downto 0);
  write_enable : in std_logic;
  q            : out std_logic_vector (15 downto 0));
END COMPONENT;

COMPONENT hexdecode IS
PORT( A : IN std_logic_vector(3 downto 0);
			D : OUT std_logic_vector(0 to 6));
END COMPONENT;

SIGNAL BusSig : STD_LOGIC_VECTOR(15 downto 0);
SIGNAL PC : STD_LOGIC_VECTOR(7 downto 0);
SIGNAL memOut : STD_LOGIC_VECTOR(15 downto 0);
SIGNAL nullSig : STD_LOGIC_VECTOR(15 downto 0);

BEGIN

p0 : processor port map (memOut(15 downto 0), KEY(1), KEY(0), memOut(15 downto 13), memOut(6 downto 4), memOut(2 downto 0), LEDG(0), BusSig(15 downto 0), LEDR(15 downto 0), PC);

-- FIRST GENERIC IS NO. OF INSTRUCTIONS, SECOND IS ADDRESS WIDTH
pm1 : progMem16 generic map(10, 8) port map(KEY(0), open, open, PC, '0',  memOut(15 downto 0));

h0 : hexdecode port map (BusSig(3 downto 0), HEX0(0 to 6));
h1 : hexdecode port map (BusSig(7 downto 4), HEX1(0 to 6));
h2 : hexdecode port map (BusSig(11 downto 8), HEX2(0 to 6));
h3 : hexdecode port map (BusSig(15 downto 12), HEX3(0 to 6));
h4 : hexdecode port map ('0'& memOut(2 downto 0), HEX4(0 to 6));
h6 : hexdecode port map ( '0' & memOut(6 downto 4), HEX6(0 to 6));
h7 : hexdecode port map ("0000", HEX5(0 to 6));
h5 : hexdecode port map ("0000", HEX7(0 to 6));

LEDG(7 downto 5) <= memOut(15 downto 13);
memview <= memOut;
viewbus <= busSig;

END structural;