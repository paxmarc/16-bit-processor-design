LIBRARY ieee;
USE ieee.std_logic_1164.all;
entity registerA is
    port( CLK, RIN : IN STD_LOGIC;
        busIn : IN STD_LOGIC_VECTOR(15 downto 0);
        output : OUT STD_LOGIC_VECTOR(15 downto 0));
end registerA;

architecture structural of registerA is
signal storage : std_logic_vector(15 downto 0);
begin

    process(RIN, CLK)
    begin
        if rising_edge(CLK) AND RIN = '1' THEN
            storage <= busIn;
        end if;
    end process;

    output <= storage;

end structural;