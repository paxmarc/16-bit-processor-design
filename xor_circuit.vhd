LIBRARY ieee;
USE ieee.std_logic_1164.all;
entity xor_circuit IS
    PORT( A, B : IN STD_LOGIC_VECTOR(15 downto 0);
        xor_out : OUT STD_LOGIC_VECTOR(15 downto 0));
END xor_circuit;

ARCHITECTURE behaviour of xor_circuit IS
BEGIN
    GEN_XOR:
    FOR I in 0 to 15 GENERATE
        xor_out(I) <= A(I) XOR B(I);
    END GENERATE GEN_XOR;
END behaviour;