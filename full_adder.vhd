LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY full_adder IS 
    PORT ( a, b, cin : IN std_logic; 
    cout, s : OUT std_logic); 
END full_adder; 

ARCHITECTURE behaviour OF full_adder IS  
BEGIN  
    s <= (a xor b) xor cin; 
    cout <= (a and b) or (cin and a) or (cin and b); 
END behaviour;             
